from django.db import models
from django.utils.translation import ugettext_lazy as _

from running_city.teams.models import Team


class Task(models.Model):
    """
    Task model
    """
    longitude = models.DecimalField(_('Долгота'), max_digits=8, decimal_places=3)
    latitude = models.DecimalField(_('Широта'), max_digits=8, decimal_places=3)
    description = models.TextField(_('Описание задачи'))
    tip1 = models.TextField(_('Первая подсказка'))
    tip2 = models.TextField(_('Вторая подсказка'))
    tip3 = models.TextField(_('Третья подсказка'))
    answer = models.CharField(_('Правильный ответ'), max_length=255)

    class Meta:
        verbose_name = _('Задание')
        verbose_name_plural = _('Задания')

    def check_answer(self, answer):
        """
        Check team answer
        :param answer: which should be checked
        :return: correct or not
        :rtype: bool
        """
        return self.asnwer.to_lower() == str(answer).to_lower()


class TeamAnswer(models.Model):
    """
    Team answers
    """
    team = models.ForeignKey(Team)
    task = models.ForeignKey(Task)
    answered_at = models.DateTimeField(_('Ответ был дан'))
    answer = models.CharField(_('Ответ команды'), max_length=255)
    is_correct = models.BooleanField(_('Правильный ли ответ?'))

    class Meta:
        verbose_name = _('Ответ команды')
        verbose_name_plural = _('Ответы команд')

    def save(self, *args, **kwargs):
        self.is_correct = self.task.check_answer(self.answer)
        super().save(*args, **kwargs)


class TeamPenalty(models.Model):
    team = models.ForeignKey(Team, related_name='penalties')
    points = models.IntegerField(_('Суммарный штраф'), default=0)

    class Meta:
        verbose_name = _('Штраф команды')
        verbose_name_plural = _('Штрафы команды')
