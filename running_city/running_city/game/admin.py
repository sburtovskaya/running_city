from django.contrib import admin

from running_city.game.models import Task, TeamAnswer, TeamPenalty


@admin.register(Task)
class Task(admin.ModelAdmin):
    pass


@admin.register(TeamAnswer)
class TeamAnswer(admin.ModelAdmin):
    pass


@admin.register(TeamPenalty)
class TeamPenalty(admin.ModelAdmin):
    pass
