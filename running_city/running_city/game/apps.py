from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class GameConfig(AppConfig):
    name = 'running_city.game'
    verbose_name = _('Игра')
