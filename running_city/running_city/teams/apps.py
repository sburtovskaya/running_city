from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TeamConfig(AppConfig):
    name = 'running_city.teams'
    verbose_name = _('Команды')
