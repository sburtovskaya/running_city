from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _


class Team(AbstractUser):
    full_name = models.CharField(_('Полное название команды'), max_length=100, blank=False, null=True)

    class Meta:
        verbose_name = _('Команда')
        verbose_name_plural = _('Команды')

    def get_full_name(self):
        """
        Return name of the ream or default string
        """
        return self.full_name or _('Вы в команде')
