from django import forms
from django.utils.translation import ugettext_lazy as _

from running_city.teams.models import Team


class RegistrationForm(forms.ModelForm):
    KEYS_MAPPING = {
        'username': 'Логин',
        'passwords': 'Пароль',
        'full_name': 'Имя команды',
    }

    class Meta:
        model = Team
        fields = ('username', 'password', 'full_name')

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        for k, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = _('обязательное поле')
