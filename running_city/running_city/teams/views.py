from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth import authenticate, login as django_login
from django.db.models import Count, Max, Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from running_city.teams.forms import RegistrationForm
from running_city.game.models import TeamAnswer, Task
from running_city.teams.models import Team


def index(request):
    """
    Main page
    """
    now = datetime.now()
    data = {
        'is_started': True,
        'is_finished': False,
        'results': [],
    }
    if now < settings.START_GAME_DATETIME:
        return render(request, 'teams/index.html', {'is_started': False})
    if now > settings.END_GAME_DATETIME:
        data['is_finished'] = True
    print(data['is_finished'])
    correct_answers = {
        answer.team.id: answer for answer in TeamAnswer.objects
        .filter(is_correct=True)
        .annotate(
            num_tasks=Count('task'),
            last_attempt=Max('answered_at')
        )
        .select_related('team')
    }
    teams = Team.objects.all().annotate(penalty=Sum('penalties__points'))
    results = []
    tasks_count = Task.objects.all().count()
    default_time = settings.END_GAME_DATETIME - settings.START_GAME_DATETIME + tasks_count * (
        3 * settings.SHOW_TIPS_PENALTY + settings.WRONG_ANSWER_PENALTY
    )
    for team in teams:
        tasks = correct_answers[team.id].num_tasks if team.id in correct_answers else 0
        results.append({
            'name': team.get_full_name(),
            'tasks': tasks,
            'time': (
                correct_answers[team.id].last_attempt -
                settings.START_GAME_DATETIME
                + timedelta(minutes=team.penalty)
            ) if team.id in correct_answers else default_time
        })
    data['results'] = sorted(results, key=lambda x: (x['tasks'], -x['time']))
    return render(request, 'teams/index.html', data)


def login(request):
    """
    View for login
    """
    if request.method != 'POST':
        return HttpResponseRedirect('/')
    username = request.POST.get('username')
    password = request.POST.get('password')
    if not username or not password:
        return HttpResponse(_('Введите логин и пароль'))
    request.session.set_expiry(0)
    user = authenticate(username=username, password=password)
    if user is None or not user.is_active:
        return HttpResponse(_('Неверный логин или пароль'))
    django_login(request, user)
    return HttpResponse('ok')


def register(request):
    """
    Register new team
    """
    if request.method != 'POST':
        return HttpResponseRedirect('/')
    form = RegistrationForm(request.POST)
    if not form.is_valid():
        return_data = []
        for key, errors in form.errors.items():
            if any(map(lambda x: 'already exists' in x, errors)):
                return_data.append('Команда с таким логином уже зарегистрировалась')
            else:
                return_data.append('{0} {1}'.format(RegistrationForm.KEYS_MAPPING[key], errors[0]))
        return HttpResponse('<br />'.join(return_data))
    team = form.save()
    django_login(request, team)
    return HttpResponse('ok')


def tasks(request):
    """
    View for tasks
    """
    return render(request, 'teams/tasks.html', {'tasks': Task.objects.all()})

"""
def submit_attempt():
    calculate penalty and answers

def show_tips():
    return next tips and calculate penalty
"""
