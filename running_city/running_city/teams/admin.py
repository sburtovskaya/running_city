from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from running_city.teams.models import Team


@admin.register(Team)
class TeamAdmin(UserAdmin):
    list_display = ['username', 'full_name', 'is_staff']
    fieldsets = (
        (None, {'fields': ('username', 'password', 'full_name')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'full_name', 'password1', 'password2'),
        }),
    )
