$(function() {
  function make_request(url, formId, alertId) {
    $.ajax({
      'url': url,
      'type': 'POST',
      'data': $('#' + formId).serialize(),
      'headers': { "X-CSRFToken": $.cookie("csrftoken") },
    }).done(function(data) {
      if (data == "ok") {
        window.location = window.location;
      } else {
        $('#' + alertId).html(data).removeAttr('hidden');
      }
    });
  }
  $('#submitRegistration').click(function(event) {
    event.preventDefault();
    make_request('/register/', 'formRegistration', 'registrationAlert');
  })
  $('#submitSignin').click(function(event) {
    event.preventDefault();
    make_request('/login/', 'formSignin', 'signinAlert');
  })
});